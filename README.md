### Prerequisites

Python 3.4

### Installing and starting the project

#### 1. Create and activate python virtual environment

virtualenv -p python3 venv_patient_registery  
source venv_patient_registery/bin/activate


#### 2. Install required python dependencies

In project root directory run:  
pip install -r requirements.txt


#### 3. Run server

In project root directory run:  
python start.py

Project is running on http://127.0.0.1:8080/


### Api doc

#### Patient list
GET http://127.0.0.1:8080/patient

#### Single patient object
GET http://127.0.0.1:8080/patient/patient_id

#### To add patient
POST http://127.0.0.1:8080/patient
```
{
    "first_name": "Monika",
    "last_name": "Drozd",
    "country":"Poland",
    "date_of_birth":"2000-08-09"
}
```

#### To remove patient object
DELETE http://127.0.0.1:8080/patient/patient_id

#### Carer list
GET http://127.0.0.1:8080/carer

#### Single carer object
GET http://127.0.0.1:8080/carer/carer_id

#### To add carer
POST http://127.0.0.1:8080/carer
```
{
    "first_name": "Monika",
    "last_name": "Drozd"
}
```
#### To remove carer
DELETE http://127.0.0.1:8080/carer/carer_id

#### Patient carer list
GET http://127.0.0.1:8080/patient/patient_id/carer

#### To add carer to the patient
POST http://127.0.0.1:8080/patient/patient_id/carer/carer_id

#### To remove carer from patient carer list
DELETE http://127.0.0.1:8080/patient/patient_id/carer/carer_id

