import Vue from 'vue';
import patient_list from './patient_list.vue';
import carer_list from './carer_list.vue';
import VueRouter from 'vue-router';
import 'bulma';


const routes = [
  { path: '/', name:'patient_list', component: patient_list },
  { path: '/carers', name:'carer_list', component: carer_list }
];

const router = new VueRouter({
  routes
});

Vue.use(VueRouter);


new Vue({
    router,
    el: '#app'
});

