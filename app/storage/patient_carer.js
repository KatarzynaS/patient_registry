import Storage from './base.js'


class PatientCarerStorage extends Storage {
    constructor() {
        super();
        this.objects = [];
        this.url = 'patient/patient_id/carer'
    }

    get(patient_id) {
        let url = this.url.replace('patient_id', patient_id);

        super.get(url, (data) => {
            this.objects = data;
        });
    }

    add(patient_id, carer_id) {
        let url = this.url.replace('patient_id', patient_id) + '/' + carer_id;

        super.post_without_payload(url, () => {
            this.get(patient_id);
        });
    }

    remove(patient_id, carer_id) {
        let url = this.url.replace('patient_id', patient_id) + '/' + carer_id;

        super.remove(url, () => {
            this.get(patient_id);
        });
    }
}

export default new PatientCarerStorage();