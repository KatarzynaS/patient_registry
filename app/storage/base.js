class Storage {

    constructor() {
        this.objects = [];
        this.ERROR_MESSAGE = 'Error occurred';
    }

    get(url, on_success) {
        fetch(url, {
            method: "GET"
        }).then((response) => {
            return this.is_success(response);
        }).then(function (response) {
            return response.json();
        }).then((data) => {
            on_success(data);
        }).catch((error) => {
            alert(this.ERROR_MESSAGE)
        });
    }

    post(url, payload, on_success) {
        fetch(url, {
            headers: {
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify(payload)
        }).then((response) => {
            return this.is_success(response);
        }).then(function (response) {
            return response.json();
        }).then((data) => {
            on_success(data);
        }).catch((error) => {
            alert(this.ERROR_MESSAGE)
        });
    }

    post_without_payload(url, on_success) {
        fetch(url, {
            method: "POST"
        }).then((response) => {
            return this.is_success(response);
        }).then(() => {
            on_success();
        }).catch((error) => {
            alert(this.ERROR_MESSAGE)
        });
    }

    remove(url, on_success) {
        fetch(url, {
            method: "DELETE",
        }).then((response) => {
            this.is_success(response);
        }).then(() => {
            on_success();
        }).catch((error) => {
            alert(this.ERROR_MESSAGE)
        });
    }

    is_success(response) {
        if (response.status >= 200 && response.status < 300) {
            return response;
        } else {
            throw new Error(response.statusText);
        }
    }
}

export default Storage;