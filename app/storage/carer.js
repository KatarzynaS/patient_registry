import Storage from './base.js'


class CarerStorage extends Storage{
    constructor() {
        super();
        this.objects = [];
        this.url = 'carer'
    }

    get() {
        super.get(this.url, (data) => {
            this.objects = data;
        });
    }

    add(payload) {
        super.post(this.url, payload, (data) => {
            this.get();
        });
    }

    remove(object_id) {
        let url = this.url + '/' + object_id;
        super.remove(url, () => {
            this.get();
        });
    }
}

export default new CarerStorage();