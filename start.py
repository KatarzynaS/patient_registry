from flask import Flask
from flask_restful import Api

from api.carer import CarerResource, CarerListResource
from api.patient import PatientResource, PatientListResource
from api.patient_carer import PatientCarerListResource, PatientCarerResource
from database.seed import create_sample_data
from db_base import db

app = Flask(__name__, static_url_path='/static')
api = Api(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


with app.app_context():
    db.init_app(app)
    db.drop_all()
    db.create_all()
    create_sample_data(db.session)
    db.session.commit()


@app.route("/")
def index():
    return app.send_static_file('index.html')


api.add_resource(PatientListResource, '/patient',)
api.add_resource(PatientResource, '/patient/<patient_id>')
api.add_resource(CarerListResource, '/carer',)
api.add_resource(CarerResource, '/carer/<carer_id>')
api.add_resource(PatientCarerListResource, '/patient/<patient_id>/carer')
api.add_resource(PatientCarerResource, '/patient/<patient_id>/carer/<carer_id>')


if __name__ == '__main__':
    app.run(port=8080, threaded=True)