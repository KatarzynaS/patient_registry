from datetime import datetime

from database.models import Patient
from repo.base import RepoBase

PATIENT_DATE_FORMAT = '%Y-%m-%d'


class PatientRepo(RepoBase):
    def get(self):
        results= self._session.query(
            Patient
        ).all()

        return [self._to_dict(result) for result in results]

    def get_one(self, patient_id):
        result = self._session.query(
            Patient
        ).filter(
            Patient.id == patient_id
        ).one()

        return self._to_dict(result)

    def add(self, data):
        date = datetime.strptime(data['date_of_birth'], PATIENT_DATE_FORMAT)
        patient = Patient(
            first_name=data['first_name'],
            last_name=data['last_name'],
            country=data['country'],
            date_of_birth=date
        )

        self._session.add(patient)
        self._session.commit()
        return patient.id

    def delete(self, patient_id):
        patient = self._session.query(
            Patient
        ).filter(
            Patient.id == patient_id
        ).one()

        self._session.delete(patient)
        self._session.commit()

    def _to_dict(self, result):
        return {
            'id': result.id,
            'first_name': result.first_name,
            'last_name': result.last_name,
            'country': result.country,
            'date_of_birth': result.date_of_birth.strftime(PATIENT_DATE_FORMAT)
        }

    # result = self._session.query(
    #     Patient.first_name,
    #     Patient.last_name,
    #     Patient.date_of_birth,
    # ).join(
    #     PatientCarer,
    #     PatientCarer.patient_id == Patient.id
    # ).join(
    #     Carer,
    #     Carer.id == PatientCarer.carer_id
    # ).all()
