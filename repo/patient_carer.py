from database.models import PatientCarer, Carer
from repo.base import RepoBase


class PatientCarerRepo(RepoBase):
    def get(self, patient_id):
        results = self._session.query(
            Carer
        ).join(
            PatientCarer,
            PatientCarer.carer_id == Carer.id
        ).filter(
            PatientCarer.patient_id == patient_id
        ).all()

        return [self._to_dict(result) for result in results]

    def add(self, patient_id, carer_id):
        patient_carer = PatientCarer(
            patient_id=patient_id,
            carer_id=carer_id,
        )

        self._session.add(patient_carer)
        self._session.commit()

    def delete(self, patient_id, carer_id):
        patient_carer = self._session.query(
            PatientCarer
        ).filter(
            PatientCarer.patient_id == patient_id,
            PatientCarer.carer_id == carer_id
        ).one()

        self._session.delete(patient_carer)
        self._session.commit()

    def _to_dict(self, result):
        return {
            'id': result.id,
            'first_name': result.first_name,
            'last_name': result.last_name
        }
