from database.models import Carer
from repo.base import RepoBase


class CarerRepo(RepoBase):
    def get(self):
        results= self._session.query(
            Carer
        ).all()

        return [self._to_dict(result) for result in results]

    def get_one(self, carer_id):
        result = self._session.query(
            Carer
        ).filter(
            Carer.id == carer_id
        ).one()

        return self._to_dict(result)

    def add(self, data):
        carer = Carer(
            first_name=data['first_name'],
            last_name=data['last_name']
        )

        self._session.add(carer)
        self._session.commit()
        return carer.id

    def delete(self, carer_id):
        carer = self._session.query(
            Carer
        ).filter(
            Carer.id == carer_id
        ).one()

        self._session.delete(carer)
        self._session.commit()

    def _to_dict(self, result):
        return {
            'id': result.id,
            'first_name': result.first_name,
            'last_name': result.last_name
        }