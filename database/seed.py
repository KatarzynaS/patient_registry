import datetime

from database.models import Patient, Carer, PatientCarer


def create_sample_data(session):
    session.add(
        Patient(id=1,
                first_name='Alexandra',
                last_name='Wasko',
                country='Poland',
                date_of_birth=datetime.date(2000,12,10))
    )
    session.add(
        Patient(id=2,
                first_name='Jan',
                last_name='Kowalski',
                country='Poland',
                date_of_birth=datetime.date(2000,12,10))
    )
    session.add(Carer(id=1, first_name='Emilia', last_name='Podkarpacka'))
    session.add(Carer(id=2, first_name='Tom', last_name='Wesierski'))
    session.add(Carer(id=3, first_name='Magda', last_name='Nowak'))
    session.commit()

    session.add(PatientCarer(patient_id=1, carer_id=1))
    session.add(PatientCarer(patient_id=2, carer_id=2))
    session.add(PatientCarer(patient_id=1, carer_id=3))
    session.add(PatientCarer(patient_id=2, carer_id=3))

    session.commit()