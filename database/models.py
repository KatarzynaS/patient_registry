from sqlalchemy import Date

from db_base import db


class Patient(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    country = db.Column(db.String(50), nullable=False)
    date_of_birth = db.Column(Date, nullable=False)


class Carer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)


class PatientCarer(db.Model):
    patient_id = db.Column(db.Integer, db.ForeignKey(Patient.id,  ondelete='CASCADE'), primary_key=True)
    carer_id = db.Column(db.Integer,db.ForeignKey(Carer.id,  ondelete='CASCADE'), primary_key=True)
