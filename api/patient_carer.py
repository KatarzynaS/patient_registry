from flask_restful import Resource
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from repo.patient_carer import PatientCarerRepo


class PatientCarerResource(Resource):
    def __init__(self):
        self._patient_carer_repo = PatientCarerRepo()

    def delete(self, patient_id, carer_id):
        try:
            self._patient_carer_repo.delete(patient_id, carer_id)
        except NoResultFound:
            return '', 404
        return '', 204

    def post(self, patient_id, carer_id):
        try:
            self._patient_carer_repo.add(patient_id, carer_id)
        except IntegrityError:
            return '', 400
        return '', 201


class PatientCarerListResource(Resource):
    def __init__(self):
        self._patient_carer_repo = PatientCarerRepo()

    def get(self, patient_id):
        carers = self._patient_carer_repo.get(patient_id)
        return carers

