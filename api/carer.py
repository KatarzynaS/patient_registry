from flask import request
from flask_restful import Resource
from sqlalchemy.orm.exc import NoResultFound

from api.validation import ValidationMixin
from repo.carer import CarerRepo

SCHEMA = {
    "type": "object",
    "properties": {
        "first_name": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "last_name": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        }
    },
    "required": ["first_name", "last_name"]
}


class CarerResource(Resource):
    def __init__(self):
        self._carer_repo = CarerRepo()

    def get(self, carer_id):
        try:
            carer = self._carer_repo.get_one(carer_id)
        except NoResultFound:
            return '', 404
        return carer

    def delete(self, carer_id):
        try:
            self._carer_repo.delete(carer_id)
        except NoResultFound:
            return '', 404
        return '', 204


class CarerListResource(Resource, ValidationMixin):
    def __init__(self):
        self._carer_repo = CarerRepo()

    def get(self):
        carers = self._carer_repo.get()
        return carers

    def post(self):
        data = request.get_json()
        self.validate(data, SCHEMA)
        carer_id = self._carer_repo.add(data)
        return carer_id, 201
