from flask_restful import abort
from jsonschema import ValidationError, FormatChecker, validate


class ValidationMixin:
    def validate(self, data, schema):
        try:
            validate(data, schema, format_checker=FormatChecker())
            return
        except ValidationError as ex:
            abort(400, message=ex.message)