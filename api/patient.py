from flask import request
from flask_restful import Resource
from sqlalchemy.orm.exc import NoResultFound

from api.validation import ValidationMixin
from repo.patient import PatientRepo

SCHEMA = {
    "type": "object",
    "properties": {
        "first_name": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "last_name": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "country": {
            "type": "string",
            "minLength": 1,
            "maxLength": 50
        },
        "date_of_birth": {
            "type": "string",
            "format": "date"
        }
    },
    "required": ["first_name", "last_name", "country", "date_of_birth"]
}


class PatientResource(Resource):
    def __init__(self):
        self._patient_repo = PatientRepo()

    def get(self, patient_id):
        try:
            patient = self._patient_repo.get_one(patient_id)
        except NoResultFound:
            return '', 404
        return patient

    def delete(self, patient_id):
        try:
            self._patient_repo.delete(patient_id)
        except NoResultFound:
            return '', 404
        return '', 204


class PatientListResource(Resource, ValidationMixin):
    def __init__(self):
        self._patient_repo = PatientRepo()

    def get(self):
        patients = self._patient_repo.get()
        return patients

    def post(self):
        data = request.get_json()
        self.validate(data, SCHEMA)
        patient_id = self._patient_repo.add(data)
        return patient_id, 201
